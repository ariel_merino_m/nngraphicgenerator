#!/usr/bin/env python
# coding: latin-1

from functions import *


def main():
    names = generate_input_names()
    init_data_grid()

    load_data()

    graph()

if __name__ == '__main__':
    main()
