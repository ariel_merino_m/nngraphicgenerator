#!/usr/bin/env python
# coding: latin-1

from config import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator


time_sum = [[], [], [], [], []]
time_min = [[], [], [], [], []]
time_max = [[], [], [], [], []]
time_avg = [[], [], [], [], []]
data_ind = [[], [], [], [], []]
indexes = [[], [], [], [], []]
brute = [[], [], [], [], []]

SECONDS = 86400.0
days = {'l': [0], 'm': [1], 'w': [2], 'j': [3], 'v': [4], 'a': [0, 1, 2, 3]}
days2 = {0: 'l', 1: 'm', 2: 'w', 3: 'j', 4: 'v'}
colors = {'l': 'r', 'm': 'b', 'w': 'g', 'j': 'm', 'v': 'k', 'a': 'k'}

total_sum = [0]
count_values = [0]
average_value = [0]


def generate_input_names():
    names = []
    for month in MONTHS:
        name = month
        name = DATA_FOLDER + '/' + name + '-' + DIRECTION + '.txt'
        names.append(name)
    return names


def init_data_grid():
    lapsus = LAPSUS * 60;
    day = DAY

    blocks = SECONDS / lapsus
    selected_days = days[day]

    if blocks > int(blocks):
        blocks = int(blocks) + 1
    else:
        blocks = int(blocks)

    for d in selected_days:
        index = 0
        for i in range(0, blocks):
            time_sum[d].append(0)
            time_max[d].append(0)
            time_min[d].append(0)
            time_avg[d].append(0)
            data_ind[d].append(0)
            indexes[d].append(((float(index) * float(LAPSUS) * 60.0) / 86400.0)*24.0)
            index += 1


def load_avg():
    for d in days[DAY]:
        for i in range(0, len(time_sum[d])):
            if data_ind[d][i] != 0:
                time_avg[d][i] = float(time_sum[d][i]) / data_ind[d][i]




def load_data():
    names = generate_input_names()
    for name in names:
        file_ = open(name, 'r')
        time = 0
        for line in file_:
            if line[0] == 'F':
                data = line.split("\t")
                day = days[data[7]][0]
                if day in days[DAY]:
                    hour_in_sec = 3600 * int(data[4]) + 60 * int(data[5]) + int(data[6])
                    index = hour_in_sec / (LAPSUS * 60)

                    time /= 60.0

                    # manejo de time_min
                    if time_min[day][index] == 0 and time != 0:
                        time_min[day][index] = time
                    elif time_min[day][index] > time:
                        time_min[day][index] = time

                    # manejo de time_max
                    if time_max[day][index] < time:
                        time_max[day][index] = time

                    # manejo de time sum
                    time_sum[day][index] += time
                    data_ind[day][index] += 1

                    brute[day].append([hour_in_sec, time])

                    total_sum[0] += (time)
                    count_values[0] = count_values[0] + 1

            else:
                data = line.split("\t")
                time = int(float(data[3]))
        file_.close()
    load_avg()


def graph():

    average_value[0] = total_sum[0] / count_values[0]

    print total_sum[0]
    print count_values[0]
    print average_value[0]

    y = np.asarray([average_value[0], average_value[0]])
    x = np.asarray([INITIAL_HOUR, FINAL_HOUR])
    avg_mark, = plt.plot(x, y, '-k', linewidth=1, label=AVG_MARK)

    for d in days[DAY]:
        x_avg = np.asarray(time_avg[d])
        y_avg = np.asarray(indexes[d])
        x_avg[x_avg == 0] = np.nan
        avg, = plt.plot(y_avg, x_avg, 'o'+colors[days2[d]], linewidth=3, label=AVG_LEGEND)

        x_min = np.asarray(time_min[d])
        y_min = np.asarray(indexes[d])
        x_min[x_min == 0] = np.nan
        min, = plt.plot(y_min, x_min, ':'+colors[days2[d]], linewidth=1, label=MIN_LEGEND)

        x_max = np.asarray(time_max[d])
        y_max = np.asarray(indexes[d])
        x_max[x_max == 0] = np.nan
        max, = plt.plot(y_max, x_max, '--'+colors[days2[d]], linewidth=1, label=MAX_LEGEND)

    avg_legend = plt.legend(handles=[max, avg, min, avg_mark])

    plt.title(TITLES[DAY], fontsize=TITLE_FONT_SIZE)

    plt.xlabel(X_LABEL, fontsize=LABEL_FONT_SIZE)
    plt.ylabel(Y_LABEL, fontsize=LABEL_FONT_SIZE)

    plt.axis([INITIAL_HOUR, FINAL_HOUR, MIN_TIME, MAX_TIME])
    plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))


    plt.gca().add_artist(avg_legend)

    plt.show()

