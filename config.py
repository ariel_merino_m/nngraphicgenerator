#!/usr/bin/env python
# coding: latin-1

DATA_FOLDER='./Data'
MONTHS = ['/semana/03-MARZO', '/semana/04-ABRIL', '/semana/05-MAYO', '/semana/06-JUNIO', '/semana/07-JULIO', '/semana/08-AGOSTO', '/semana/09-SEPTIEMBRE', '/semana/10-OCTUBRE', '/semana/11-NOVIEMBRE', '/semana/12-DICIEMBRE', '/viernes/03-MARZO', '/viernes/04-ABRIL', '/viernes/05-MAYO', '/viernes/06-JUNIO', '/viernes/07-JULIO', '/viernes/08-AGOSTO', '/viernes/09-SEPTIEMBRE', '/viernes/10-OCTUBRE', '/viernes/11-NOVIEMBRE', '/viernes/12-DICIEMBRE']
DIRECTION = 'I'  # I - V
DAY = 'a'  # l (lunes) - m (martes) - w (miercoles) - j (jueves) - v (viernes) - a (todos)

INITIAL_HOUR = 4  # hora h24
FINAL_HOUR = 24  # hora h2

MIN_TIME = 0  # minutos
MAX_TIME = 120  # minutos

LAPSUS = 10  #minutos entre muestra


LEGEND_FONT_SIZE = 12
MIN_LEGEND='Minimum travel time'
MAX_LEGEND='Maximum travel time'
AVG_LEGEND='Average travel time'
AVG_MARK='Average travel time regardless of the start time'

LABEL_FONT_SIZE = 10
Y_LABEL = 'Total travel time [minutes]'
X_LABEL = 'Start time [hour]'


TITLE_FONT_SIZE = 14
TITLES = {
    'l': 'Monday',
    'm': 'Tuesday',
    'w': 'Wednesday',
    'j': 'Thursday',
    'v': 'Friday',
    'a': 'All week'
}
