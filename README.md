# NNGraphicGenerator

Generador de gr�ficos de comparaci�n de tiempo para profesor Victor Parada

## Requisitos
* Python 2.X
* Librer�a matplotlib
* Librer�a numpy
* Librer�a Python-tk
* Archivos de entrada (Data)
* Sistema operativo Linux

## Instalaci�n de librer�as
las librer�as necesarias para generar los gr�ficos pueden ser instaladas completamente con el comando mostrado a continuaci�n:

*$ sudo apt install python2.7 python-matplotlib python-tk*

para verificar la instalaci�n es posible usar el comando:

*$ python --version*

## Configuraci�n del generador de gr�ficos
En la carpeta nngraphicgenerator est� el archivo *config.py* este archivo contiene todas las variables modificables del script.

* para seleccionar el d�a a graficar es necesario modificar el valor de la variable DAY en el archivo *config.py*. Los valores que esta puede tomar son siempre caracteres en m�nusculas y se listan a continuaci�n:

	1. 'l' : graficar el d�a lunes
	2. 'm' : graficar el d�a martes
	3. 'w' : graficar el d�a mi�rcoles
	4. 'j' : graficar el d�a jueves
	5. 'v' : graficar el d�a viernes
	6. 'a' : gr�fico compuesto de todos los d�as de la semana

* dentro de estas se muestran las siguientes configuraciones generales:

	1. *LAPSUS*: Indica el tiempo entre cada muestra en minutos, seg�n el cual se calculan los promedios de los tiempos de viaje. *(default = 10)*
	2. *DIRECTION*: Indica el sentido del viaje utilizado para el an�lisis. *(default = 10)*
	3. *INITIAL_HOUR*: Indica el horario inicial [Horas], o l�mite inferior, del eje X del gr�fico. (debe ser un n�mero entero entre 0 y 24) *(default = 4)*
	4. *FINAL_HOUR*: Indica el horario final [Horas] o l�mite superior, del eje X del gr�fico. (debe ser un n�mero entero entre 0 y 24) *(default = 24)*
	5. *MIN_TIME*: Indica el tiempo m�nimo de viaje a graficar [Minutos] o l�mite superior del eje Y. (debe ser un n�mero entero) *(default = 0)*
	6. *MAX_TIME*: Indica el tiempo m�ximo de viaje a graficar [Minutos] o l�mite superior del eje Y. (debe ser un n�mero entero) *(default = 120)*

* tambi�n es posible modificar las etiquetas a mostrar en el gr�fico, Para esto existe el siguiente grupo de variables

	### GRUPO-1: *LEYENDAS*

	1. *MIN_LEGEND*: indica el texto a mostrar en la leyenda que describe el gr�fico de tiempo m�nimo
	2. *MAX_LEGEND*: indica el texto a mostrar en la leyenda que describe el gr�fico de tiempo m�ximo
	3. *AVG_LEGEND*: indica el texto a mostrar en la leyenda que describe el gr�fico de puntos de tiempo promedio
	4. *AVG_MARK*: indica el texto a mostrar en la leyenda que describe la l�nea horizontal con el tiempo promedio del d�a

	### GRUPO-2: *EJES*

	1. *LABEL_FONT_SIZE*: indica el tama�o del texto de las etiquetas de los ejes
	2. *Y_LABEL*: indica el texto que muestra la etiqueta del eje Y
	3. *X_LABEL*: indica el texto que muestra la etiqueta del eje X

	### GRUPO-3: *T�TULO*

	1. *TITLE_FONT_SIZE*: Indica el tama�o del texto en el t�tulo del gr�fico
	2. *TITLES*: Contiene un diccionario que describe el t�tulo que llevar� el gr�fico seg�n el d�a indicado:
	
		2.1. 'l': T�tulo correspondiente al d�a lunes
		
		2.2. 'm': T�tulo correspondiente al d�a martes
		
		2.3. 'w': T�tulo correspondiente al d�a mi�rcoles
		
		2.4. 'j': T�tulo correspondiente al d�a jueves
		
		2.5. 'v': T�tulo correspondiente al d�a viernes
		
		2.6. 'a': T�tulo correspondiente al gr�fico que incluye todos los d�as

## EJECUCION
para generar los gr�ficos es necesario ejecutar la siguiente l�nea de comandos:

*$ python /ruta/a/carpeta/nngraphiggenerator/main.py*
		
#IMPORTANTE#

La estructura de *config.py* **NO** debe ser modificada, si los valores tienen comillas simples ' o dobles " deben mantenerse, al igual que las asignaciones con =, : o {}. Cualquier otro cambio se encuetra permitido




